.PHONY: build fix-psr2
.SILENT: build

build:
	rm -f ./generate

	php --define phar.readonly=0 ./build.php
	mv generate.phar generate
	chmod +x generate

	echo "Done."
