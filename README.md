### Generator CLI options

|short option|long option|description|file|
|------------|-----------|-----------|----|
|H|hostgroup|Host groups count. If host count defined all created hosts will be spread equally within new host groups.|HostGroupGenerator.php|
|U|usergroup|User groups count. If user count defined all created users will be spread equally within new user groups.|UserGroupGenerator.php|
|T|template|Templates count. If template count defined all created hosts will be spread equally within new templates.|TemplateGenerator.php|
|h|host|Hosts count.|HostGenerator.php|
|u|user|Users count.|UserGenerator.php|
|i|item|Items count to be created for every host.|HostItemGenerator.php|
|t|trigger|Trigger count to be created for every host. Requires item count and host count to be defined. Every new trigger will use one by one item within it host, item used in the different trigger will match only if defined triggers count is greater than items count.|HostTriggerGenerator.php|
|g|graph|Count of graph to be created for new items. (!) If count of graphs is less than items count there will be hosts without graphs. When additional option contain comma character, value will be interpreted as list of values and set for every graph in round-robin style.|HostGraphGenerator.php|
|a|application|Applications count to be created for every host.|HostApplicationGenerator.php|
|p|proxy|Proxies count to be created.|ProxyGenerator.php|
||data|Generate items data.|HostItemDataGenerator.php|
|m|map|Create map with generated elements.|MapGenerator.php NestedMapGenerator.php|
||prefix|String to be used as prefix part of name for generated: host, host group, item, graph, map, application, proxy.||
||bulk|Cached items count, default 100.||
||timeout|Connection timeout seconds, default 900.||
|v||Show information for every API calls.||
|vv||More information about generator current action steps.||

### Additional options
**Template generation:**

|option|description|
|------|-----------|
|tids|Template ids (string of integers separated by comma character) to be linked every created template to.|

**Host generation:**

|option|description|
|------|-----------|
|tids|Template ids (string of integers separated by comma character) to be linked every created host to. This option is ignored, if templates are beeing generated.|

**Item generation**

|option|default value|description|
|-------|-------------|-----------|
|item_type|2|Item type. Default value "Zabbix trapper" item.|
|value_type|0|Item value type. Default value "numeric float".|
|delay|30s|Item update delay value seconds.|
|params||Calculated item formula.|
|tags||Comma separated list of tags to add to created items where single tag is written in form "{tag}={value}"|
|preproc_type||TBD|
|preproc_params||TBD|

Create 1 hostgroup with 1 host and 300 items where half are trapper items and half are calculated. Calculated items have value_type 4 and two different formula "2+2" or "1+1":
```
API_URL="http://z/4.4/frontends/php/api_jsonrpc.php" ./generate -H1 -h1 -i300 --item_type="2,15" --value_type="0,4" --params="-,2+2,-,1+1"  -v --bulk=2000 --prefix="api-22-"
```

Create 1 hostgroup with 1 host and 10 items. Result in 4 items have tag1=value1, 3 items no tag and 3 items with tag3=value3
```
API_URL="http://z.git/master/ui/api_jsonrpc.php" ./generate -H1 -h1 -i10 --tags="tag1=value1, ,tag3=value3"
```

**Trigger generation:**

|option|default value|descriptioin|
|------|-------------|------------|
|function|last(),prev()|Functions to be used for created triggers. Support dynamic placeholder replacement for host name and item key. Part of dynamic placeholders "%0" set relative offset and "$h" or "$i" defines host or item level placeholder should be used for replacement. Relative offset should be in interval 0-9.|
|priority|0,1,2|Severity of the trigger.|

Generate 1 host group, 1 host, 100 items and 50 triggers. 25 triggers will have expression for one item and function "prev()", other 25 triggers will have expression based on generated host name and item key.
```
API_URL="http://z/4.0/frontends/php/api_jsonrpc.php" ./generate -H1 -h1 -i100 -t50 --function='{Zabbix server:agent.ping.last()} and {%0$h:%0$i.last()} and {%0$h:%1$i.last()},prev()' -v
```

**Graph generation:**

|option|default value|description|
|------|-------------|-----------|
|width|320|Graph width.|
|height|240|Graph height.|
|graphtype|0,1,2|Graph type.|
|percent_left||Left percentile.|
|percent_right||Right percentile.|
|show_3d|0,1|Whether to show pie and exploded graphs in 3D.|
|show_legend|0,1|Whether to show the legend on the graph.|
|show_work_period|0,1|Whether to show the working time on the graph.|
|color|1A7C11,F63100,2774A4,A54F10,|Color values for graph items.|
||FC6EA3,6C59DC,AC8C14,611F27,F230E0,||
||CCD18,BB2A02,5A2B57,89ABF8,7EC25C,||
||274482,2B5429,8048B4,FD5434,790E1F,87AC4D,E89DF4||
|max||Limit count of items to be used on one graph.|

Count defines count of graphs for all items generated for all hosts, not for single host. This is done such way to allow to create graphs with items from different hosts. Option "--max" only limit items count on every generated graph. To be able to create 1 graph for every generated host "count" should be equal to host count and "--max" should not be used.

**Proxy generation:**

|option|default value|description|
|------|-------------|-----------|
|status|5,6|proxy status|
|dns|localhost,|proxy interface dns field value|
|ip|,127.0.0.1|proxy interface ip field value|
|useip|0,1|proxy interface useip field value|
|port|10051|proxy interface port field value|

**Item data generation:**

|option|default value|description|
|------|-------------|-----------|
|itemid||Trapper item id. If no value set generated item ids will be used.|
|min|0|Minimum value, integer.|
|max|100|Maximum value, integer.|
|server|127.0.0.1|Zabbxi server host.|
|port|10051|Zabbix server port.|
|from|-1 month|Item data date range start.|
|to|now|Item data date range end.|

Options 'to' and 'from' can use relative date format described [here](https://www.php.net/manual/en/datetime.formats.relative.php).

Generate 50 values for item with id equal 23877 for interval from -3 hour till now:
```
API_URL="http://localhost/branches/4.0/frontends/php/api_jsonrpc.php" ./generate --itemid=23877 --data=50 --from="-3 hour"
```
Create 10 hosts with 10 items on each host and generate 100 values for every item with interval staring "-1 hour" till now. Zabbix server is listening on port 10040
```
API_URL="http://localhost/branches/4.0/frontends/php/api_jsonrpc.php" ./generate -h10 -i10 --data=100 --port=10040 --from="-1 hour"
```

**Map and submap generation:**

|option|default value|description|
|------|-------------|-----------|
|icon_off|​Cloud_(64),Disk_array_2D_(64),House_(64),|List of icons for map element|
||IP_PBX_(64),Network_adapter_(64)||
|width|800|Map width|
|height|600|Map height|
|cols|8|Columns count used to calculate map element step X axis. *maximal width of colon is 100 pixels when using default.*|
|rows|6|Rows count used to calculate map elements step on Y axis. *maximal height of colon is 100 pixels when using default.*|
|submap||Defines depth of nested maps. *Example: 1 - no nested maps, 2 - one map will be added to another map as submap, 3 - two maps will be added as submaps.*

**User generation:**

|option|default value|description|
|------|-------------|-----------|
|type|1|Type of created users|

Create 2 user groups with 5 users in each group. Half of users will be with type Zabbix admin.
```
API_URL="http://z/branches/4.0/frontends/php/api_jsonrpc.php" ./generate -U2 -u10 --type="1,2"
```

**User Group generation:**

|option|default value|description|
|------|-------------|-----------|
|permissions|3 *(read-write access)*|User group host permissions.|

Create 2 host groups with 1 host in each group and 2 user groups with 1 user in group with permission "read-only" and "read-write".
```
API_URL="http://z/branches/4.0/frontends/php/api_jsonrpc.php" ./generate -H2 -h2 -U2 -u2 --permissions="2,3"
```


### Environment variables
(!) Even data generation for single item requires `API_URL` to be set!

|variable|description|
|--------|-----------|
|API_URL|**Required**! URL to json_rpc.php file.|
|API_USER|API user username value, default "Admin".|
|API_PASSWORD|API user password value, default "zabbix".|

### Examples

Example usage after checkout:
```
cd src
API_URL="http://z/branches/4.0/frontends/php/api_jsonrpc.php" ./generate -H10 -h20 -i5 -a20
```

Generate one host with problems:
```
API_URL="http://z/branches/4.0/frontends/php/api_jsonrpc.php" ./generate -H1 -h1 -i7 -t7 --function="last()" --data=50 --min=0 --max=5 --from="-1 hour" --port=10040
```

### Building .phar

Use make task to build singlefile executable. File will be saved in local folder with `generate` filename.
```
make build
```

### Notes

Observed strange behavior with encoding under Windows with PHP5 cli therefore is recommended to use PHP7+ under Windows.
