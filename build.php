<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


$src_root = './src';
$phar_file = 'generate.phar';

$phar = new Phar($phar_file);
$phar->startBuffering();
$main = $phar->createDefaultStub('generate.php');
$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($src_root), RecursiveIteratorIterator::SELF_FIRST);

foreach ($dir as $file) {
	if (preg_match('/\\.php$/i', $file)) {
		$phar->addFromString(substr($file, strlen($src_root) + 1), php_strip_whitespace($file));
	}
	elseif (preg_match('/\\.json$/i', $file)) {
		$phar->addFromString(substr($file, strlen($src_root) + 1), json_encode(json_decode(file_get_contents($file))));
	}
}

$phar->setStub("#!/usr/bin/php \n".$main);
$phar->stopBuffering();
