<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Api;

/**
 * JSONRPC service consumer.
 *
 * Example usage:
 *
 *     $api = new Api('http://z/branches/4.0/frontends/php/api_jsonrpc.php');
 *     $api->auth = $api->userLogin(['user' => 'Admin', 'password' => 'zabbix']);
 *     $items = $api->itemGet(['limit' => 1]);
 *
 *     $api = new Api('http://z/branches/4.0/frontends/php/api_jsonrpc.php');
 *     $auth = $api->call('user.login', ['user' => 'Admin', 'password' => 'zabbix']);
 *     $items = $api->call('item.get', ['limit' => 1], $auth);
 */
class Api {
	// Incremental request identifier.
	public static $requestid = 1;

	// API request additional options.
	public static $stream_context = [
		'ssl' => [
			'verify_peer' => false
		]
	];

	// Active Api class instance.
	public static $active_instance = null;

	// API request headers.
	public static $headers = [
		'Content-Type: application/json',
		'Accept: application/json'
	];

	// Auth token used for API requests which require authentication.
	public $auth;

	// URL path to jsrpc.php API file. http://localhost/branches/4.0/frontends/php/jsrpc.php
	public $url;

	// Api verbose level.
	public $verbose = 0;

	protected $timeout = 900;

	/**
	 * Get active instace of Api class.
	 */
	public static function getActiveInstance() {
		return Api::$active_instance;
	}

	public function __construct($url, $auth = null) {
		$this->url = $url;
		$this->auth = $auth;
		Api::$active_instance = &$this;
	}

	/**
	 * Set connection timeout value.
	 *
	 * @param int $value    Connection timeout seconds.
	 */
	public function setTimeout($value) {
		$this->timeout = $value;
	}

	/**
	 * Echo message when API verbose is set.
	 *
	 * @param string $message    Message.
	 * @param int    $verbose    Message verbose level value.
	 */
	protected function message($message, $verbose = 1) {
		if ($verbose > $this->verbose) {
			return;
		}

		echo date('H:i:s'), ' ', $message, "\n";
	}

	/**
	 * Make API request.
	 *
	 * @param string $method    Request method. example: api.apiinfo, item.get
	 * @param array  $params    Request params.
	 * @param string $auth      Auth token.
	 *
	 * @throws ApiException
	 *
	 * @return array
	 */
	public function call($method, $params, $auth = null) {
		if ($auth === null) {
			$auth = $this->auth;
		}
		else {
			$this->message('HTTP auth token for request changed to '.$auth, 2);
		}

		$id = static::$requestid++;
		$jsonrpc = '2.0';
		$request = static::$stream_context + [
			'http' => [
				'timeout' => $this->timeout,
				'method' => 'POST',
				'header' => implode("\r\n", static::$headers),
				'content' => json_encode(compact('jsonrpc', 'method', 'params', 'auth', 'id')),
			]
		];

		$this->message('HTTP send data to API '.$method.', using timeout '.$this->timeout.'s.');
		$this->message(
			'HTTP request #'.$id.":\n".
			json_encode(compact('jsonrpc', 'method', 'params', 'auth', 'id'), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE),
			2
		);
		$response_string = @file_get_contents($this->url, false, stream_context_create($request));

		$response = json_decode($response_string, true);

		if (!is_array($response)) {
			throw new ApiException([
				'message' => 'Unable to parse response to JSON.',
				'data' => $response_string,
				'code' => null
			], compact('jsonrpc', 'method', 'params', 'auth', 'id'));
		}
		elseif (array_key_exists('error', $response)) {
			throw new ApiException($response['error'], compact('jsonrpc', 'method', 'params', 'auth', 'id'));
		}

		$this->message("HTTP response:\n".json_encode($response, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE), 2);

		return $response['result'];
	}

	/**
	 * Translates call to non exisitng method with camel case name to API request having 'method' in dot notation.
	 */
	public function __call($method, $arguments = []) {
		$arguments = array_pad($arguments, 2, null);
		$method[0] = strtolower($method[0]);

		array_unshift($arguments, preg_replace_callback('/([A-Z])/', function ($match) {
			return '.' . strtolower($match[1]);
		}, $method));

		return call_user_func_array([$this, 'call'], $arguments);
	}
}
