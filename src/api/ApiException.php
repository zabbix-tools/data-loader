<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Api;

use \Exception;

class ApiException extends Exception {
	private $response;
	private $request;

	public function __construct(array $response, array $request) {
		$this->response = $response;
		$this->request = $request;
		parent::__construct($response['message'].' '.$response['data'], $response['code']);
	}

	/**
	 * Get response as array.
	 *
	 * @return array
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * Get request as array
	 *
	 * @return array
	 */
	public function getRequest() {
		return $this->request;
	}
}
