<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Api;

/**
 * Bulk request api.
 */
class CachedApi extends Api {
	protected $cache = [];
	protected $bulk_size = 0;

	/**
	 * Setter for maximal count of items in cached array.
	 *
	 * @param int $size    Size of bulk request.
	 *
	 * @return CachedApi
	 */
	public function setBulkSize($size) {
		$this->bulk_size = $size;

		return $this;
	}

	/**
	 * Envoke API call on cached entities and returns created entity ids.
	 *
	 * @param string $entity    Entity name.
	 * @param string $key       If defined not whole API response value will be returned, only value of property
	 *                          defined by key.
	 *
	 * @throws ApiException
	 *
	 * @return array
	 */
	public function flush($entity, $key = null) {
		$result = [];

		if (array_key_exists($entity, $this->cache) && $this->cache[$entity]) {
			$this->message('Creating '.count($this->cache[$entity]).' '.$entity.' items');

			$result = parent::call($entity.'.create', $this->cache[$entity]);
			$this->cache[$entity] = [];

			if ($key !== null) {
				return $result[$key];
			}
		}
		else {
			$this->message('HTTP flush request ignored, no data to flush.', 2);
		}

		return $result;
	}

	/**
	 * Cache entity create requests. Will return empty array when create is cached, use flush to envoke API call.
	 *
	 * @param string $method    Request method. example: api.apiinfo, item.get
	 * @param array  $params    Request params.
	 * @param string $auth      Auth token.
	 *
	 * @throws ApiException
	 *
	 * @return array
	 */
	public function call($method, $params, $auth = null) {
		list($entity, $action) = explode('.', $method, 2);
		$result = [];

		if ($action === 'create') {
			if ($auth !== null) {
				throw new ApiException([
					'message' => 'Cached requests should not contain auth token.',
					'data' => null,
					'code' => null
				], []);
			}

			if (!array_key_exists($entity, $this->cache)) {
				$this->cache[$entity] = [];
			}

			// Params can be single entity or array of entities.
			if (array_values($params) !== $params) {
				$params = [$params];
			}

			$this->cache[$entity] = array_merge($this->cache[$entity], $params);

			if (count($this->cache[$entity]) >= $this->bulk_size) {
				$result = $this->flush($entity);
			}
		}
		else {
			$this->message('API request '.$method);

			$result = parent::call($method, $params, $auth);
		}
		
		return $result;
	}
}
