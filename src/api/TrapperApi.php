<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Api;

/**
 * Server interaction api.
 */
class TrapperApi {
	protected $cache = [];
	protected $bulk_size = 0;
	protected $timeout = 900;
	public $verbose = 0;

	public $host;
	public $port;

	public function __construct($host, $port) {
		$this->host = $host;
		$this->port = $port;
		$this->message('Sender initialization '.$host.':'.$port);
	}

	/**
	 * Setter for maximal count of items in cached array.
	 *
	 * @param int $size    Size of bulk request.
	 *
	 * @return TrapperApi
	 */
	public function setBulkSize($size) {
		$this->bulk_size = $size;

		return $this;
	}

	/**
	 * Set connection timeout value.
	 *
	 * @param int $value    Connection timeout seconds.
	 */
	public function setTimeout($value) {
		$this->timeout = $value;
	}

	/**
	 * Echo message when API verbose is set.
	 *
	 * @param string $message    Message.
	 * @param int    $verbose    Message verbose level value.
	 */
	protected function message($message, $verbose = 1) {
		if ($verbose > $this->verbose) {
			return;
		}

		echo date('H:i:s'), ' ', $message, "\n";
	}

	/**
	 * Send data from cache to server, makes physical connection to server via socket to transmit data.
	 * Items data cache will be cleared on success only.
	 */
	public function flush() {
		if (!$this->cache) {
			return;
		}

		$this->message('Sender flush cache.');
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

		// Set socket timeouts to 10sec.
		if ($socket) {
			socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => $this->timeout, 'usec' => 0]);
			socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => $this->timeout, 'usec' => 0]);
		}

		if (@socket_connect($socket, $this->host, $this->port)) {
			$count = count($this->cache);
			$this->message('Sender send '.$count.' items data to server '.$this->host.':'.$this->port.
				', using timeout '.$this->timeout.'s.');
			$body = json_encode([
				'request' => 'sender data',
				'data' => $this->cache
			]);
			$body = "ZBXD\1".pack('V', strlen($body))."\0\0\0\0".$body;

			if (socket_send($socket, $body, strlen($body), 0) !== false) {
				$response = '';
				// Returns first 1024 bytes of data from socket.
				socket_recv($socket, $response, 1024, 0);
				socket_close($socket);
				// Ignore response header data (13bytes).
				$data = json_decode(mb_substr($response, 13), true);

				if ($data === null || $data['response'] !== 'success') {
					throw new ApiException([
						'message' => 'Failed to use response data. Response string:',
						'data' => $response,
						'code' => null
					], $this->cache);
				}

				if (array_key_exists('info', $data)) {
					list($processed, $failed, $total) = sscanf($data['info'], 'processed: %d; failed: %d; total %d');
					$this->message('Sender status: '.$data['info'], 2);

					if ($failed > 0) {
						throw new ApiException([
							'message' => 'Server failed to add '.$failed.' data entries.',
							'data' => null,
							'code' => null
						], $this->cache);
					}
				}

				$this->cache = [];

				return $data;
			}
		}

		throw new ApiException([
			'message' => socket_strerror(socket_last_error()),
			'data' => '',
			'code' => socket_last_error()
		], []);
	}

	/**
	 * Send data to server.
	 *
	 * Single item array keys:
	 * string 'host'       Item host name.
	 * string 'key'        Item key value.
	 * mixed  'value'      Item value.
	 * int    'clock'      Item timestamp value, optional.
	 * int    'ns'         Item nanoseconds value, optional.
	 *
	 * @param array $data    Data to be sent, can be single item array or array of items data.
	 */
	public function call(array $data) {
		$result = [];
		// Params can be single entity or array of entities.
		if (array_values($data) !== $data) {
			$data = [$data];
		}

		$this->cache = array_merge($this->cache, $data);

		if (count($this->cache) >= $this->bulk_size) {
			$result = $this->flush();
		}

		return $result;
	}
}
