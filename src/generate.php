<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\CachedApi;
use Zabbix\DataLoader\Api\ApiException;
use \Exception;

global $argv;

$config = json_decode(file_get_contents('config.json'), true);
$global_options = $config['globals'];
$options = array_reduce(array_column($config['generators'], 'options'), 'array_merge', []);
$params = array_reduce(array_column($config['generators'], 'params'), 'array_merge', []);
$options = array_keys(array_flip(array_merge($params, $options, array_keys($global_options))));
$exit_code = 0;
$input = [];
$unsupported_option = false;

foreach (array_slice($argv, 1) as $arg) {
	if ($arg[1] === '-') {
		list($label, $value) = explode('=', substr($arg, 2), 2);
	}
	else {
		list($label, $value) = [$arg[1], substr($arg, (isset($arg[2]) && $arg[2] === '=') ? 3 : 2)];
	}

	if (array_search($label, $options) === false && !$unsupported_option) {
		$unsupported_option = true;
		$exit_code = 1;
		echo 'Invalid option ',$arg,"\n\n";
	}

	$input[$label] = $value;
}

$global_options = array_intersect_key($input, $global_options) + $global_options;

if (array_key_exists('v', $input)) {
	$global_options['verbose'] = substr_count($global_options['v'], 'v') + 1;
}

if (!$input || $unsupported_option) {
	$options = '';
	foreach ($config['generators'] as $generator) {
		$short = $long = [];

		foreach ($generator['options'] as $gen_option) {
			if (strlen($gen_option) > 1) {
				$long[] = '--'.$gen_option;
			}
			else {
				$short[] = '-'.$gen_option;
			}
		}
		$short = implode(' ', $short);
		$long = implode(' ', $long);

		$options .= sprintf('%1$-24s', trim($short.' '.$long))."\t".$generator['description']."\n";

		if ($generator['params']) {
			$options .= str_repeat(' ', 24)."\tAdditional parameters: ".implode(', ', $generator['params'])."\n";
		}
	}

	echo <<<HELP
Usage:
{$argv[0]} -options

Global options:

-v                Verbose, show steps currently made by generator.
-vv               More verbose, show body of API requests.
--bulk            Size of bulk for API/Server requests, default 100.
--timeout         HTTP and socket connection seconds timeout, default 900.

Generator options:

{$options}

Environment variables:

API_URL           Required.
API_USER          API user username value, default "Admin"
API_PASSWORD      API user password value, default "zabbix"


HELP;
	exit($exit_code);
}

if (getenv('API_URL') === false) {
	echo "Please define API URL as value of API_URL environment variable!\n";
	exit(1);
}

include 'api/Api.php';
include 'api/CachedApi.php';
include 'api/TrapperApi.php';
include 'api/ApiException.php';
include 'generators/GeneratorAbstract.php';

$api_url = getenv('API_URL');

if ($api_url !== '' && substr($api_url, -4) !== '.php') {
	$api_url = rtrim($api_url, '/').'/api_jsonrpc.php';
}

$api_user = getenv('API_USER') === false ? 'Admin' : getenv('API_USER');
$api_pass = getenv('API_PASSWORD') === false ? 'zabbix' : getenv('API_PASSWORD');

echo 'Logging in as ',$api_user,"\n";
$api = new CachedApi($api_url);
$api->verbose = $global_options['verbose'];
$api->setBulkSize($global_options['bulk']);
$api->setTimeout($global_options['timeout']);
try {
	$api->auth = $api->userLogin(['user' => $api_user, 'password' => $api_pass]);
}
catch (ApiException $e) {
	$response = $e->getResponse();

	echo implode("\n", [
		'Authentication failed!',
		'Request:',
		json_encode($e->getRequest(), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE),
		''
	]);

	if (is_array($response) && array_key_exists('data', $response)) {
		echo implode("\n", [
			'Raw response ('.gettype($response['data']).'):',
			is_string($response['data']) ? $response['data'] : json_encode($response['data']),
			''
		]);
	}

	exit(1);
}
$time_spent = [];

try {
	foreach ($config['generators'] as $generator) {
		$count = array_intersect_key($input, array_fill_keys($generator['options'], ''));
		$params = array_intersect_key($input, array_fill_keys($generator['params'], ''));

		if (empty($count)) {
			continue;
		}

		$option = key($count);
		$count = reset($count);
		$class = substr($generator['class'], strrpos($generator['class'], '\\') + 1);

		if ($option !== '' && (!ctype_digit($count) || $count == 0)) {
			echo 'Error: ',$class,' incorrect value "',$count,'" for option "',$option,'", no data will be generated for this option.',
				"\n";
		}
		else if ($count > 0) {
			$time_spent[$class] = time();
			echo 'Running ',$class,"\n";
			include $generator['path'];
			$gen = new $generator['class']();
			$gen->setParamsDefault($generator['default'] + $global_options);
			$gen->setParams($params);
			$gen->run($api, $count);
			$time_spent[$class] = time() - $time_spent[$class];
		}
	}
} catch (ApiException $e) {
	echo implode("\n", [
		'Error!',
		'Request:',
		json_encode($e->getRequest(), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE),
		'Message: '.$e->getMessage(),
		'Code: '.$e->getCode(),
		''
	]);

	$exit_code = 1;
	$time_spent[$class] = time() - $time_spent[$class];
} catch (Exception $e) {
	echo "Error: {$e->getMessage()}\n";

	$exit_code = 1;
	$time_spent[$class] = time() - $time_spent[$class];
}

echo 'Logging out ',$api_user,"\n";
$time_spent = ['Total time: ' => array_sum($time_spent)] + $time_spent;
foreach ($time_spent as $label => $seconds) {
	echo sprintf("\t%s %.2f (%d sec).\n", $label, $seconds/60, $seconds);
}

try {
	$api->userLogout([]);
} catch (ApiException $e) {
	$response = $e->getResponse();
	$exit_code = 1;

	echo implode("\n", [
		'User log out action failed!',
		'Request:',
		json_encode($e->getRequest(), JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE),
		'Message: '.$e->getMessage(),
		'Code: '.$e->getCode(),
		''
	]);

	if (is_array($response) && array_key_exists('data', $response)) {
		echo implode("\n", [
			'Raw response ('.gettype($response['data']).'):',
			is_string($response['data']) ? $response['data'] : json_encode($response['data']),
			''
		]);
	}
}

exit($exit_code);
