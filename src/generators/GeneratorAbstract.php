<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use \Exception;

/**
 * Base class for all generators.
 */
abstract class GeneratorAbstract {
	// Generator instances.
	protected static $generators = [];

	// Array of generators class names on whom current generator depends.
	public $depends = [];

	// Name pattern used to generate enitity name with sprintf.
	public $name_pattern = '';

	// Generated data.
	protected $data = [];

	// Additional parameters.
	protected $params = [];

	// Default values for additional parameters.
	protected $default = [];

	public function __construct() {
		self::$generators[] = $this;
	}

	/**
	 * Every generator entry point.
	 *
	 * @param Api $api      Initialized Api object.
	 * @param int $count    Count of entries to be generated.
	 */
	public function run(Api $api, $count) {
		throw new Exception(get_class($this).' not implemented.');
	}

	/**
	 * Set additional parameters to be used by generator.
	 *
	 * @param array $params    Associative array of parameters.
	 */
	final public function setParams(array $params) {
		$this->params = $params;
	}

	/**
	 * Set additional parameters default values to be used by generator.
	 *
	 * @param array $default   Associative array of default values.
	 */
	final public function setParamsDefault(array $default) {
		$this->default = $default;
	}

	/**
	 * Get generator additional parameters.
	 *
	 * @return array
	 */
	final public function getParams() {
		return $this->params;
	}

	/**
	 * Get generator additional parameters default values.
	 *
	 * @return array
	 */
	final public function getParamsDefault() {
		return $this->default;
	}

	/**
	 * Get generator additional parameter by parameter name. Returns null if parameter was not set.
	 *
	 * @param string $key    Desired parameter key.
	 *
	 * @return mixed
	 */
	final public function getParam($key) {
		$params = array_merge($this->getParamsDefault(), $this->getParams());

		return array_key_exists($key, $params) ? $params[$key] : null;
	}

	/**
	 * Every call will return desired parameters with new value using round-robin algorithm.
	 *
	 * @param array $keys    Array of parameter keys to be returned.
	 *
	 * @return array
	 */
	public function getParamsRoundRobin(array $keys) {
		static $indexes = [];

		$params = array_merge($this->getParamsDefault(), $this->getParams());
		$params = array_intersect_key($params, array_fill_keys($keys, ''));
		$result = [];

		foreach ($params as $key => $param) {
			if (strpos($param, ',') === false) {
				$result[$key] = $param;
				continue;
			}

			$index = array_key_exists($key, $indexes) ? $indexes[$key] : 0;
			// https://stackoverflow.com/a/6243797
			$value = preg_split('/\\\\.(*SKIP)(*FAIL)|,/s', $param, -1, PREG_SPLIT_NO_EMPTY);
			$result[$key] = $value[array_key_exists($index, $value) ? $index : $index%count($value)];
			$indexes[$key] = $index + 1;
		}
		unset($param);

		return $result;
	}

	/**
	 * Every call will return desired parameter new value using round-robin algorithm.
	 *
	 * @param string $key     Desired parameter key.
	 *
	 * @return mixed
	 */
	public function getParamRoundRobin($key) {
		$result = $this->getParamsRoundRobin([$key]);

		return array_key_exists($key, $result) ? $result[$key] : null;
	}

	/**
	 * Get registered generators data.
	 *
	 * @param string $class   Generator class name.
	 *
	 * @return array|null
	 */
	final public function getData($class = null) {
		if ($class === null) {
			if ($this->data === null && $this->getParam('remote') && method_exists($this, 'getRemoteDataByPrefix')) {
				$this->data = $this->getRemoteDataByPrefix(Api::getActiveInstance(), $this->getParam('remote'));
			}

			return $this->data;
		}

		$elm = reset(self::$generators);

		while ($elm !== false && get_class($elm) !== $class) {
			$elm = next(self::$generators);
		}

		return $elm !== false ? $elm->getData() : null;
	}

	/**
	 * Prompt for user input.
	 *
	 * @param string $message    Prompt message.
	 */
	public function prompt($message) {
		echo $message, PHP_EOL;

		$handle = fopen('php://stdin', 'r');

		fgets($handle);

		fclose($handle);
	}
}
