<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use \Exception;

/**
 * Generate host applications, requires hosts generated by HostGenerator.
 */
class HostApplicationGenerator extends GeneratorAbstract {
	public $name_pattern = '';
	public $index = 1;

	/**
	 * @see GeneratorAbstract::run()
	 */
	public function run(Api $api, $count) {
		$this->data = [];
		$total = $count;
		$hostids = $this->getData(HostGenerator::class);

		if ($hostids === null) {
			throw new Exception('HostApplicationGenerator no host ids found, cannot generate applications.');
		}

		$hostids = array_keys($hostids);
		$this->name_pattern = $this->getParam('prefix').'app-%0'.strlen((string) $count).'d-host-%0'.
				strlen((string) max($hostids)).'d';

		$hostid = reset($hostids);

		while ($hostid !== false) {
			$count = $total;
			$appids = [];

			while ($count > 0) {
				$result = $api->applicationCreate([
					'name' => sprintf($this->name_pattern, $this->index, $hostid),
					'hostid' => $hostid
				]);

				if ($result) {
					$appids = array_merge($appids, $result['applicationids']);
				}

				--$count;
				++$this->index;
			}

			$this->data[$hostid] = array_merge($appids, $api->flush('application', 'applicationids'));
			$this->index -= $total;
			$hostid = next($hostids);
		}
	}
}
