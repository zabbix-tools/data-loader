<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use \Exception;

/**
 * Generate host groups.
 */
class HostGroupGenerator extends GeneratorAbstract {
	public $name_pattern = '';
	public $index = 1;

	/**
	 * @see GeneratorAbstract::run()
	 */
	public function run(Api $api, $count) {
		$this->data = [];
		$this->name_pattern = $this->getParam('prefix').'hostgroup-%0'.strlen((string) $count).'d';

		while ($count > 0) {
			$result = $api->hostgroupCreate([
				'name' => sprintf($this->name_pattern, $this->index)
			]);

			if ($result) {
				$this->data = array_merge($this->data, $result['groupids']);
			}

			--$count;
			++$this->index;
		}

		$this->data = array_merge($this->data, $api->flush('hostgroup', 'groupids'));
	}
}
