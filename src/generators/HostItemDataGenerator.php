<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use Zabbix\DataLoader\Api\TrapperApi;
use \Exception;

/**
 * Generate host trapper type items data using socket connection to server, requires host items generated
 * by HostItemGenerator or '--itemid' command line option with valid trapper type item ids.
 */
class HostItemDataGenerator extends GeneratorAbstract {
	public $name_pattern = null;
	public $index = null;

	/**
	 * @see GeneratorAbstract::run()
	 */
	public function run(Api $api, $count) {
		$min = $this->getParam('min');
		$max = $this->getParam('max');
		$from = strtotime($this->getParam('from'));
		$to = strtotime($this->getParam('to'));

		$sender = new TrapperApi($this->getParam('server'), $this->getParam('port'));
		$bulk = $this->getParam('bulk');
		$sender->setBulkSize($bulk);
		$sender->verbose = (bool) $this->getParam('verbose');

		if ($this->getParam('itemid') === null) {
			$itemids = $this->getData(HostItemGenerator::class);

			if ($itemids === null) {
				throw new Exception('HostItemDataGenerator no item ids found, cannot generate data.');
			}

			$itemids = array_keys($itemids);
			$this->prompt('Before data generation please reload server cache with "zabbix_server -R config_cache_reload" and press any key.');
		}
		else {
			$itemids = [$this->getParam('itemid')];

			$item = $api->itemGet([
				'output' => ['type'],
				'itemids' => $itemids
			]);

			if (!$item) {
				throw new Exception('HostItemDataGenerator no permissions to referred object or it does not exist.');
			}

			if ($item[0]['type'] !== '2') {
				throw new Exception('HostItemDataGenerator only trapper type items are supported.');
			}
		}

		while ($itemids) {

			// ITEM_TYPE_TRAPPER
			$items = $api->itemGet([
				'output' => ['key_', 'hostid'],
				'filter' => ['type' => 2],
				'itemids' => array_splice($itemids, 0, $bulk)
			]);

			if ($items) {
				$hosts = $api->hostGet([
					'output' => ['host'],
					'hostids' => array_unique(array_column($items, 'hostid')),
					'preservekeys' => true
				]);
			}

			for ($i = $count, $mul = ($to - $from) / $count; $i > 0; $i--) {
				foreach ($items as $item) {
					$sender->call([
						'host' => $hosts[$item['hostid']]['host'],
						'key' => $item['key_'],
						'value' => rand($min, $max),
						'clock' => $to - round($i * $mul)
					]);
				}
			}
		}

		$sender->flush();
	}
}
