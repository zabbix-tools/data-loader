<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use \Exception;

/**
 * Generate maps, uses host groups generated by HostGroupGenerator, hosts generated by HostGenerator and triggers
 * generated by HostTriggerGenerator.
 */
class MapGenerator extends GeneratorAbstract {
	public $name_pattern = '';
	public $index = 1;

	/**
	 * @see GeneratorAbstract::run()
	 */
	public function run(Api $api, $count) {
		$this->data = [];
		$this->name_pattern = 'map-%0'.strlen((string) $count).'d';
		$hostids = $this->getData(HostGenerator::class);
		$hostids = ($hostids !== null) ? array_keys($hostids) : null;
		$collections = [
			'hostgroupids' => $this->getData(HostGroupGenerator::class),
			'hostids' => $hostids,
			'triggerids' => $this->getData(HostTriggerGenerator::class)
		];
		$have_data = false;
		
		foreach ($collections as $collection => &$collection_itemids) {
			if ($collection_itemids !== null) {
				$collection_itemids = array_chunk(
					$collection_itemids,
					max(1, (int) (count($collection_itemids) / $count))
				);
				$have_data = true;
			}
		}
		unset($collection_itemids);

		if (!$have_data) {
			$collections = [];
		}

		$iconids = [];
		// Build associative array of icons used by generator, array key is icon name.
		$iconids = $api->imageGet([
			'output' => ['imageid', 'name'],
			'filter' => [
				'imagetype' => 1,
				'name' => explode(',', trim($this->getParam('icon').','.$this->getParam('icon_off'), ', '))
			]
		]);
		$iconids = array_column($iconids, 'imageid', 'name');

		$col_size = $this->getColSize();
		$row_size = $this->getRowSize();

		while ($count > 0) {
			$x = $y = 0;
			$map = $this->getParamsRoundRobin(['height', 'width', 'expand_macros', 'expandproblem', 'highlight',
				'label_format', 'label_location', 'label_type', 'markelements', 'severity_min', 'show_unack',
				'show_suppressed'
			]) + [
				'selements' => [],
				'name' => $this->getParam('prefix').$this->name_pattern
			];
			$map['name'] = sprintf($map['name'], $this->index);

			foreach ($collections as $collection => &$collection_itemids) {
				if (!$collection_itemids) {
					continue;
				}

				switch ($collection) {
					case 'hostgroupids':
						$element_default = [
							'elementtype' => 3,
							'areatype' => 1
						];
						$key = 'groupid';
						break;

					case 'hostids':
						$element_default = [
							'elementtype' => 0
						];
						$key = 'hostid';
						break;

					case 'triggerids':
						$element_default = [
							'elementtype' => 2
						];
						$key = 'triggerid';
						break;
				}

				foreach (array_shift($collection_itemids) as $elementid) {
					$map['selements'][] = $element_default + [
						'elements' => [
							[$key => $elementid]
						],
						'iconid_off' => $iconids[$this->getParamRoundRobin('icon_off')]
					] + compact('x', 'y');

					$x += $col_size;

					if ($x + $col_size > $map['width']) {
						$x = 0;
						$y += $row_size;
					}
				}
			}
			unset($collection_itemids);

			$result = $api->mapCreate($map);

			if ($result) {
				$this->data = array_merge($this->data, $result['sysmapids']);
			}

			--$count;
			++$this->index;
		}

		$this->data = array_merge($this->data, $api->flush('map', 'sysmapids'));
	}

	/**
	 * Return size of column in pixels.
	 *
	 * @return int
	 */
	protected function getColSize() {
		return $this->getParam('cols') === null
			? min(100, floor($this->getParam('width') / 8))
			: floor($this->getParam('width') / $this->getParam('cols'));
	}

	/**
	 * Return size of row in pixels.
	 *
	 * @return int
	 */
	protected function getRowSize() {
		return $this->getParam('rows') === null
			? min(100, floor($this->getParam('height') / 6))
			: floor($this->getParam('height') / $this->getParam('rows'));
	}
}
