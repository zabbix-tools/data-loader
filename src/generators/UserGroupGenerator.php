<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/


namespace Zabbix\DataLoader\Generators;

use Zabbix\DataLoader\Api\Api;
use \Exception;

/**
 * Generate user groups.
 */
class UserGroupGenerator extends GeneratorAbstract {
	public $name_pattern = '';
	public $index = 1;

	/**
	 * @see GeneratorAbstract::run()
	 */
	public function run(Api $api, $count) {
		$this->data = [];
		$total = $count;
		$hostgrpids = $this->getData(HostGroupGenerator::class);
		$hg_count = ($hostgrpids === null) ? 0 : count($hostgrpids);
		$this->name_pattern = $this->getParam('prefix').'usergroup-%0'.strlen((string) $count).'d';

		while ($count > 0) {
			$rights = [];

			if ($hg_count > 0) {
				$offset = ($total - $count) % $hg_count;
	
				while ($offset < $hg_count) {
					$rights[] = [
						'id' => $hostgrpids[$offset],
						'permission' => $this->getParamRoundRobin('permissions')
					];
					$offset += $total;
				}
			}

			$result = $api->usergroupCreate([
				'name' => sprintf($this->name_pattern, $this->index),
				'rights' => $rights
			]);

			if ($result) {
				$this->data = array_merge($this->data, $result['usrgrpids']);
			}

			--$count;
			++$this->index;
		}

		$this->data = array_merge($this->data, $api->flush('usergroup', 'usrgrpids'));
	}
}
